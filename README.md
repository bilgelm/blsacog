## Installation

To install the blsacog library in R, first clone (or download) this repository
to your computer. Then, in R, execute:  
`install.packages("blsacog/", repos=NULL, type="source")`  
The first argument to `install.packages` must be the path to where you cloned
(or downloaded the repository).