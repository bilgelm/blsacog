% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/cogdomains.R
\name{anchored_zscore}
\alias{anchored_zscore}
\title{Compute anchored z-scores for a long-format longitudinal variable}
\usage{
anchored_zscore(values, anchorBoolean)
}
\arguments{
\item{values}{a numeric vector for which anchored z-scores will be computed (long-format longitudinal data for a variable; each row is a different ID-visit)}

\item{anchorBoolean}{a boolean vector (should evaluate to TRUE once per ID)}
}
\value{
a list containing four elements:
\itemize{
\item "z" is a numeric vector containing anchored z-scores
\item "mean" is the anchored mean
\item "sd" is the anchored standard deviation
\item "n" is the number of observations used to compute the mean and sd
}
}
\description{
Normally, z-scores are computed by subtracting the mean of the original variable from the original variable
and dividing this difference by the standard deviation of the original variable.
However, working with longitudinal data, we do not want to compute the mean and standard
deviation across all longitudinal observations, as this would bias the mean and standard deviation estimates towards
individuals with a greater number of visits. Mean and standard deviation should instead be computed
using a single visit per individual. anchored_zscore is a helper function towards this goal.
This function allows for the computation of z-scores by using mean and standard deviation computed
based on a pre-specified subset of the observations.
}
\seealso{
\code{\link[=cogdomain_scores]{cogdomain_scores()}} for the main function for computing cognitive domain scores based on anchored z-scores
}
