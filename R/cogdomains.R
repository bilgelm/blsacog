getElement <- function(pos, x, ...) {
  #' Wrapper function that switches the order of first two arguments to the function get
  #'
  #' @param pos same as the argument pos in [get()]
  #' @param x same as the first argument in [get()]
  #' @param ... same as the optional arguments to [get()]
  #' @return same thing as [get()]
  #' @seealso [get()]
  get(x, pos, ...)
}

anchored_zscore <- function(values, anchorBoolean) {
  #' Compute anchored z-scores for a long-format longitudinal variable
  #'
  #' Normally, z-scores are computed by subtracting the mean of the original variable from the original variable
  #' and dividing this difference by the standard deviation of the original variable.
  #' However, working with longitudinal data, we do not want to compute the mean and standard
  #' deviation across all longitudinal observations, as this would bias the mean and standard deviation estimates towards
  #' individuals with a greater number of visits. Mean and standard deviation should instead be computed
  #' using a single visit per individual. anchored_zscore is a helper function towards this goal.
  #' This function allows for the computation of z-scores by using mean and standard deviation computed
  #' based on a pre-specified subset of the observations.
  #'
  #' @param values a numeric vector for which anchored z-scores will be computed (long-format longitudinal data for a variable; each row is a different ID-visit)
  #' @param anchorBoolean a boolean vector (should evaluate to TRUE once per ID)
  #' @return a list containing four elements:
  #' - "z" is a numeric vector containing anchored z-scores
  #' - "mean" is the anchored mean
  #' - "sd" is the anchored standard deviation
  #' - "n" is the number of observations used to compute the mean and sd
  #' @seealso [cogdomain_scores()] for the main function for computing cognitive domain scores based on anchored z-scores

  # check arguments
  stopifnot(is.numeric(values))
  stopifnot(is.vector(values))
  stopifnot(is.logical(anchorBoolean))
  stopifnot(length(values)>1) # there should be at least two values
  stopifnot(length(values)==length(anchorBoolean))
  stopifnot(sum(anchorBoolean)>1) # there should be at least 2 values used for computing sd, otherwise sd=0

  obj <- list(mean=mean(values[anchorBoolean], na.rm=T),
              sd=stats::sd(values[anchorBoolean], na.rm=T),
              n=sum(!is.na(values[anchorBoolean])))
  obj$z <- (values - obj$mean) / obj$sd

  obj
}

guess_colnames <- function(column_names) {
  #' Guess column names for ID, visit, and age given a vector of column names
  #'
  #' Because there is no consistent naming convention for column names used in
  #' BLSA data spreadsheets, the column name for ID can be
  #' anything from "id" to "Idno" to "blsaid".
  #' Similarly, there are various possibilities for visit and age column names.
  #' This function tries to guess the column names for ID, visit, and age given
  #' a set of column names.
  #' Use this function at your own risk, as its output is simply a guess!
  #'
  #' @param column_names a vector of strings
  #' @return a list with names "id", "vi", and "age", with elements corresponding to the column names. NA if not found.

  # check arguments
  stopifnot(is.character(column_names))
  stopifnot(is.vector(column_names))

  # the one to match (ignorecase) first wins!
  allowed_names <- list("id"=c("idno", "blsaid", "id", "emsid", "emsfn"),
                        "vi"=c("visit", "blsavi", "vi", "visno", "emsvi"),
                        "age"=c("age"))

  matching_names <- list()
  for (col in names(allowed_names)) {
    matches <- tolower(trimws(column_names)) %in% allowed_names[[col]]
    if (any(matches)) {
      matching_names[[col]] <- column_names[min(which(matches))]
    } else {
      matching_names[[col]] <- NA_character_
    }
  }

  warning("Make sure that column name guesses are accurate!\n",
          "ID   : ", matching_names$id, "\n",
          "visit: ", matching_names$vi, "\n",
          "age  : ", matching_names$age)

  matching_names
}

get_tests_in_domain <- function() {
  #' Get a list of tests used to compute cognitive domain scores
  #' @returns a list with elements equal to the name of the cognitive domain,
  #' and vector-valued entries that contain the names of the tests used to
  #' construct the cognitive domain
  #' @export
  list("Memory"=c("CVLtca","CVLfrl"),
       "Attention"=c("neglogTRATS","DigFor"),
       "ExecFun"=c("neglogTRBTS","DigBac"),
       "Language"=c("FLUCat","FLULet"),
       "Clocks"=c("CLK325","CLK1110"),
       "Visuospatial"=c("CRDRot","Clocks"),
       "Processing Speed"=c("DSSTot"))
}

cogdomain_scores <- function(data,
                             idname = NA_character_,
                             viname = NA_character_,
                             agename = NA_character_,
                             anchor="first", allow.na=TRUE) {
  #' Compute cognitive domain scores
  #'
  #' This function computes cognitive domain scores for as many domains as possible
  #' based on availability of test scores in the provided data set.
  #' @param data a data.frame in long-format containing Idno, Visit, and cognitive test scores
  #' @param idname name of column in data that corresponds to subject ID. If not provided, the function will try to guess it.
  #' @param viname name of column in data that corresponds to visit number. If not provided, the function will try to guess it.
  #' @param agename name of column in data that corresponds to age. If not provided, the function will try to guess it.
  #' Not required unless you are trying to compute Attention, Executive Function, Clocks, or Visuospatial domain scores
  #' allowing missingness in domain score computation.
  #' @param anchor "first" or "last" (default="first")
  #' @param allow.na a boolean indicating whether domain scores should be computed even if some of the tests are missing (default=TRUE)
  #' @return a list containing two entries:
  #' - the "data" entry is a data.frame containing a boolean variable named "is.anchor" that indicates
  #'   which visits were used to compute anchored z-scores,
  #'   the anchored z-scores (indicated with a ".z" suffix in the column names), and the cognitive domain scores
  #' - the "metadata" entry is a data.frame containing the anchored means ("anchored_mean"),
  #'   standard deviations ("anchored_sd"),
  #'   and the number of observations used for computing the mean and sd ("anchored_n").
  #'   Row names are the cognitive test names from the original data set.
  #' @export

  # check arguments
  stopifnot(is.data.frame(data))

  if (any(is.na(c(idname,viname,agename)))) {
    idvi <- guess_colnames(colnames(data))
    if (is.na(idname)) {
      idname <- idvi$id
    }
    if (is.na(viname)) {
      viname <- idvi$vi
    }
    if (is.na(agename)) {
      agename <- idvi$age
    }
  }

  if (any(is.na(c(idname,viname)))) {
    stop("The algorithm could not find at least one of these variables in the data set: ID, visit. ",
         "Please specify the idname and viname arguments and rerun.")
  }

  # missing id or vi is not allowed
  if (any(is.na(data[,c(idname,viname)]))) {
    stop(paste(idname,"or", viname, "cannot be missing"))
  }
  # duplicated id vi pairs are not allowed
  if (anyDuplicated(data[,c(idname,viname)])) {
    stop(paste("duplicated",idname,viname,"pairs are not allowed"))
  }

  # sort dataset
  ordering <- order(data[,idname],data[,viname])
  data <- data[ordering,]

  if (is.character(anchor)) {
    if (anchor=="first") {
      anchorBoolean <- with(data, eval(parse(text=viname))==ave(eval(parse(text=viname)), eval(parse(text=idname)),
                                                                FUN=function(x) x[1]))
    } else if (anchor=="last") {
      anchorBoolean <- with(data, eval(parse(text=viname))==ave(eval(parse(text=viname)), eval(parse(text=idname)),
                                                                FUN=function(x) x[length(x)]))
    } else {
      stop("not a valid argument for anchor")
    }
  } else if (is.logical(anchor) && is.vector(anchor) && length(anchor)==nrow(data)) {
    # must sort anchor the same way as data set
    anchor <- anchor[ordering]

    # there should be at least two anchors
    stopifnot(sum(anchor) > 1)

    # there cannot be more than one anchor per participant
    vals <- as.numeric(table(anchor,data[,idname])["TRUE",])
    stopifnot(all(vals < 2))

    anchorBoolean <- anchor
  } else {
    stop("when specifying a custom anchor, the anchor variable must be a boolean vector with as many elements as the number of rows in data")
  }

  data[,"is.anchor"] <- anchorBoolean

  tests_in_domain <- get_tests_in_domain()

  for (trailsvar in c("TRATS","TRBTS")) {
    if (trailsvar %in% colnames(data)) {
      if (any(data[!is.na(data[,trailsvar]),trailsvar]==0)) {
        warning("There are 0 values in ",trailsvar,". 0's will be set to NA prior to log transform.")
        data[data[!is.na(data[,trailsvar]),trailsvar]==0,trailsvar] <- NA_real_
      }
      data[,paste0("neglog",trailsvar)] <- -log(data[,trailsvar])
    }
  }

  # convert each test to a z-score
  tests_available <- unlist(tests_in_domain)[unlist(tests_in_domain) %in% colnames(data)]
  zscore_objs <- apply(data[,tests_available], 2, anchored_zscore, anchorBoolean=anchorBoolean)

  anchored_mean <- sapply(zscore_objs, getElement, x="mean")
  anchored_sd <- sapply(zscore_objs, getElement, x="sd")
  anchored_n <- sapply(zscore_objs, getElement, x="n")

  anchored_zscores <- sapply(zscore_objs, getElement, x="z")
  colnames(anchored_zscores) <- paste0(colnames(anchored_zscores),".z")

  for (domain in names(tests_in_domain)) {
    if (all(tests_in_domain[[domain]] %in% colnames(data))) {
      if (domain=="Visuospatial") {
        data[,domain] <- apply(cbind(anchored_zscores[,"CRDRot.z"], data[,"Clocks"]),
                               1, mean, na.rm=allow.na)
      } else if (domain=="Processing Speed") {
        data[,domain] <- anchored_zscores[,"DSSTot.z"]
      } else {
        data[,domain] <- apply( # average across z-scores
          anchored_zscores[,paste0(tests_in_domain[[domain]],".z")],
          1, mean, na.rm=allow.na)
      }
    }
  }

  # do not compute Attention, Executive Function, Clocks, or Visuospatial for age<60 since
  # Trails and Clocks aren't administered below 60, resulting in a missingness structure that is not random.
  if (allow.na) { # if NA is not allowed for domain computation purposes, all of the domains below will be NA for individuals <60 anyway, so no need to test.
    domains_not_available_below_60 <- c("Attention","ExecFun","Clocks","Visuospatial")
    for (domain in domains_not_available_below_60) {
      if (domain %in% colnames(data)) {
        if (is.na(agename)) {
          stop("The algorithm could not find the age variable in the data set. ",
               "Please specify the agename argument and rerun.")
        }
        data[data[,agename]<60, domain] <- NA_real_
      }
    }
  }

  data.output <- cbind(data[,c(idname,viname,"is.anchor",
                               colnames(data)[colnames(data) %in% names(tests_in_domain)])],
                       data.frame(anchored_zscores))
  metadata.output <- data.frame(cbind(anchored_mean,anchored_sd,anchored_n))

  output <- list(data = data.output,
                 metadata = metadata.output)
}
